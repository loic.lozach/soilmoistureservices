# Look up how to launch docker if not running ``
echo USERUID=$UID > useruid.txt
DATAPATH=/mnt/GEOSUD_WA/SoilMoisture/_PROD
docker build --rm -t ssmmanager .
docker run -it -v $DATAPATH:/data -e TERM ssmmanager bash
