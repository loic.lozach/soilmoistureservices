# Sentinel Soil Moisture at Plot scale (S2MP) Production Services

This module contains applications for Operational Soil Moisture Mapping over Agricultural Areas with Sentinel-1 and Sentinel-2

## User manual

* The [Soil Moisture Pipeline for agricultural areas user manual](https://gitlab.irstea.fr/loic.lozach/soilmoistureservices/-/blob/master/SoilMoistureServices_Manual.pdf) is available.

* The [Soil Moisture Pipeline Download Module user manual](https://gitlab.irstea.fr/loic.lozach/soilmoistureservices/-/blob/master/SoilMoistureServices_Manual_Download_Module.pdf) is available.

# Install

L’application est encapsulée dans une image Docker, ce qui permet une grande portabilité. Toutefois certaines configurations sont spécifiques à Linux, l’installation est donc limitée à ce type d’OS.

```
lozach@cesprod-loic:~$
lozach@cesprod-loic:~$ cd repertoire_install/
lozach@cesprod-loic:~/repertoire_install$ git clone https://gitlab.irstea.fr/loic.lozach/soilmoistureservices.git
```
Dans le dossier soilmoistureservices, ouvrir le fichier build.sh et modifier la variable DATAPATH par le chemin d’accès à vos données. Ouvrir le fichier config.ini et modifier les valeurs sous [ESA] et [DOWNLOADS]. 
Pour la variable path ne pas modifier /data/ qui correspond au répertoire de montage à l’intérieur de l’image docker. Le répertoire /data/exemple/dir_sentinel/ correspondra au chemin DATAPATH/exemple/dir_sentinel/ sur votre système.
Puis lancer le script build.sh. 
Si vous souhaitez plus tard changer l’une de ces variables, il est nécessaire de relancer le build pour que les modifications soient prises en compte.
```
lozach@cesprod-loic:~$ cd repertoire_install/soilmoistureservices/
lozach@cesprod-loic:~/repertoire_install/soilmoistureservices$ ./built.sh
```
Une fois l’installation terminée, le script lance directement l’image docker ‘’ssmmanager’’ et on obtient le prompt ci-dessous.
```
To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.
otbuser@a518d80d8ce2:/data$
```

# Lancement de l’image Docker
Pour lancer l’image Docker sans avoir à relancer le script build.sh, il suffit d’afficher le contenu de celui-ci (commande ‘’cat’’) et copier les lignes qui nous intéressent.
```
lozach@cesprod-loic:~$ cd repertoire_install/soilmoistureservices/
lozach@cesprod-loic:~/repertoire_install/soilmoistureservices$ cat build.sh
echo USERID=$UID > useruid.txt
DATAPATH=/your/data/directory
docker build --rm -t ssmmanager .
docker run -it -v $DATAPATH:/data -e TERM ssmmanager bash
lozach@cesprod-loic:~/repertoire_install/soilmoistureservices$ DATAPATH=/your/data/directory
lozach@cesprod-loic:~/repertoire_install/soilmoistureservices$ docker run -it -v $DATAPATH:/data -e TERM ssmmanager bash
To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.
otbuser@a518d80d8ce2:/data$
```

# Usage

* Download Service
```
otbuser@d922b9b915cc:/data$ download_pipeline.py

```

* Calibration Service
```
otbuser@ce4dccfa13f8:/data$ calibration_service.py -h

usage: calibration_service.py [-h] -indir INDIR -inref INREF -zone ZONE
                              -outdir OUTDIR [-polar {vv,vh}]
                              [-theta {loc,elli}] [--overwrite]

S1 Calibration, orhorectification, dem, slope, aspect and incidence angle
processing

optional arguments:
  -h, --help         show this help message and exit
  -indir INDIR       Directory containing Sentinel1 .SAFE directories
  -inref INREF       Image reference, generally corresponding to a S2 tile
  -zone ZONE         Geographic zone reference for output files naming
  -outdir OUTDIR     Output directory
  -polar {vv,vh}     [Optional] Process VV or VH polarization, default vv
  -theta {loc,elli}  [Optional]Create local incidence raster or incidence
                     raster from ellipsoid, default loc
  --overwrite        [Optional] Overwrite already existing files (default
                     False)

```

* Preprocess Service
```
otbuser@ce4dccfa13f8:/data$ preprocess_service.py -h

usage: preprocess_service.py [-h]
                             {exmosS1,ndvi,gapfilling,stack,slope,gpm} ...

Preprocessing Services

positional arguments:
  {exmosS1,ndvi,gapfilling,stack,slope,gpm}
                        Preprocess pipeline
    exmosS1             Extract and mosaic Sentinel-1 to fit Sentinel-2 Tile
    ndvi                Compute cloud-masked NDVI from Sentinel2 or Landsat8.
    gapfilling          Perform Gapfilling over NDVI time serie, W
    stack               Create stacked and masked image from NDVI directory to
                        the input of a segmentation process
    slope               Extract SRTM1SecHGT with image ref and compute slope
    gpm                 Downloads, extracts and computes synthesis of NASA
                        Global Precipitation Model raster for TF model
                        selection (wet or dry)

optional arguments:
  -h, --help            show this help message and exit

```

* Production Service
```
otbuser@ce4dccfa13f8:/data$ production_service.py -h
usage: production_service.py [-h]
                             {short,mask,segment,segmentvect,serie,csvtomoist}
                             ...

Launch Soil Moisture pipeline with different level of processing.

positional arguments:
  {short,mask,segment,segmentvect,serie,csvtomoist}
                        Pipelines
    short               Inversion pipeline from user's masked labels, NDVI,
                        SarVV and SarIncidence
    mask                Mask labels image before inversion
    segment             Segment NDVI before masking and inversion
    segmentvect         Rasterize segmented NDVI vector before masking and
                        inversion
    serie               Batch inversion pipeline over a directory of
                        Sentinel-1 images
    csvtomoist          Apply CSV results on labels raster to create moisture
                        map

optional arguments:
  -h, --help            show this help message and exit

```

## License

Please see the license for legal issues on the use of the software (GNU Affero General Public License v3.0).

## Contact
Loïc Lozac'h (IRSTEA)
