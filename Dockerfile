FROM mdl4eo/otbtf1.6
SHELL ["/bin/bash", "-c"]

MAINTAINER Loic Lozach <loic.lozach[at]irstea[dot]fr>

# ----------------------------------------------------------------------------
# Build OTB SoilMoisture
# ----------------------------------------------------------------------------
USER root

ENV GIT_SSL_NO_VERIFY=true
RUN cd /work/otb/otb/Modules/Remote \
 && git clone https://gitlab.irstea.fr/loic.lozach/AgriSoilMoisture.git \
 && cd /work/otb/build/OTB/build \
 && cmake /work/otb/otb \
	-DModule_AgriSoilMoisture=ON \
	-DModule_OTBTemporalGapFilling=ON \
 && cd /work/otb/build \
 && make -j $(grep -c ^processor /proc/cpuinfo)

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update ; apt-get --assume-yes install apt-utils 
RUN apt-get --assume-yes install dialog
RUN apt-get --assume-yes install libncurses5-dev libncursesw5-dev
RUN apt-get --assume-yes install xclip

RUN mkdir /work/python
ADD requirements.txt /work/python
WORKDIR /work/python
RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt
ADD . /work/python


RUN source useruid.txt \
    && deluser --remove-home otbuser \
    && useradd -m -U -u $USERUID -G sudo otbuser

RUN chown -R otbuser:otbuser /work/python/theia_download
RUN mkdir /data && chown otbuser:otbuser /data

ENV TERM=xterm-256color
ENV PYTHONPATH=/work/python:/work/python/SentinelDM:/work/otb/superbuild_install/lib/otb/python
ENV PATH=$PATH:/work/python/sms_pipeline:/work/otb/superbuild_install/bin
ENV GDAL_DATA=/usr/share/gdal/2.2/
ENV OTB_APPLICATION_PATH=/work/otb/superbuild_install/lib/otb/applications
ENV GDAL_DRIVER_PATH="disable"
ENV LC_NUMERIC=C
RUN unset LD_LIBRARY_PATH
ENV OTB_MAX_RAM_HINT=512
RUN alias ll='ls -al'


USER otbuser
WORKDIR /data
