from django.apps import AppConfig


class SmsPipelineManagerConfig(AppConfig):
    name = 'sms_pipeline_manager'
