# import sys
# sys.path.append(r'C:\Users\ibrahim.fayad\Desktop\SentQuery')
from parsy import regex,seq,string_from,generate,fail,string,eof,whitespace
import os
from collections import namedtuple
from .shp_utils import get_convex_hull,shp_to_wkt,geojson_to_wkt
from dateutil.relativedelta import relativedelta
from datetime import datetime
import glob

__version_mjaor__ = 0
__version_minor__ = 3
__version_micro__ = 9
__version__ = "{}.{}.{}".format(__version_mjaor__, __version_minor__, __version_micro__)

class QueryParser:
    
    var = namedtuple("var", "type value")
    data_types = {
        0:'dir path',
        1:'file path',
        2:'shapefile path',
        3:'geojson path',
        4:'geo extent',
        5:'integer',
        6:'float',
        7:'date',
        8:'string',
        9:'point',
        10:'polygon',
        11:'int ragne',
        1000:'var'
        }
    var_v = lambda v: v.value
    eol = string(r"\n").many()
    eosemi = string(";").many()
    line_end = (eol >> eosemi | eosemi >> eol | eosemi >> eof | eol >> eof).result("\n").desc('End of line')
    
    def __init__(self):
        self.registered_vars = dict()
        self.registered_criteria = list()
        self.discarded = list()
        self.order_by = list()
        
        ######### Query Parsing reults
        self.query_string = ''
        self.query_order = ''
        self.discarded_from_query = ''
        self.is_query_success = False
        
        ######### Parsers
        self.dir_test = self.dir_test_fn()
        self.file_test = self.file_test_fn()
        self.string_test = self.string_test_fn()
        self.identifier = self.identifier_fn()
        self.variable_test = self.variable_test_fn()
        self.assign_char = self.assign_char_fn()
        self.decimal = self.decimal_fn()
        self.integer = self.integer_fn()
        self.coor_point = self.coor_point_fn()
        self.polygon = self.polygon_fn()
        self.type_value = self.type_value_fn()
        self.assignment_test = self.assignment_test_fn()
        self.range_test = self.range_test_fn()
        self.op = self.op_fn()
        self.date_test = self.date_test_fn()
        self.req_footprint_test = self.req_footprint_test_fn()
        self.req_platformname_test = self.req_platformname_test_fn()
        self.req_bydate_test = self.req_bydate_test_fn()
        self.req_filename_test = self.req_filename_test_fn()
        self.req_orbitnumber_test = self.req_orbitnumber_test_fn()
        self.req_orbitdirection_test = self.req_orbitdirection_test_fn()
        self.req_polarmode_test = self.req_polarmode_test_fn()
        self.req_producttype_test = self.req_producttype_test_fn()
        self.req_cloudcover_test = self.req_cloudcover_test_fn()
        self.req_discard_test = self.req_discard_test_fn()
    
    @staticmethod
    def lexeme(p):
        """
        From a parser (or string), make a parser that consumes
        whitespace on either side.
        """
        if isinstance(p, str):
            p = string(p)
        return regex(r'\s*') >> p << regex(r'\s*')
    

    def dir_test_fn(self):
        @generate('Valid directory path enclosed in single quotes')
        def dir_test_impl():
            pot_dir = yield regex(r"'[^\n\r;]+'").desc('File path')
            if os.path.isdir(pot_dir[1:-1]):
                return(QueryParser.var(0,pot_dir[1:-1]))
            return fail("valid directory")
        return dir_test_impl
    
    
    def file_test_fn(self):
        @generate('Valid file path enclosed in single quotes')
        def file_test_impl():
            pot_file = yield regex(r"'[^\n\r;]+'").desc('File path')
            if os.path.isfile(pot_file[1:-1]):
                return(QueryParser.var(1,pot_file[1:-1]))
            return fail("valid file")
        return file_test_impl
    
    
    def string_test_fn(self):
        @generate('String enclosed in single quotes')
        def string_test_impl():
            pot_string = yield regex(r"'[^\n\r;]+'")
            return(QueryParser.var(8,pot_string[1:-1]))
        return string_test_impl
    
    
    def identifier_fn(self):
        @generate('Valid identifier')
        def identifier_impl():
            v_name = yield regex(r"[a-zA-Z_][a-zA-Z_0-9]*")
            if len(v_name)>0:
                return QueryParser.var(1000,v_name)
            return fail('valid identifier name')
        return identifier_impl
    
    
    def variable_test_fn(self):
        @generate('Defined Variable')
        def variable_test_impl(expected_type=None):
            variable = yield self.identifier
            if variable.value in self.registered_vars:
                if expected_type and expected_type != variable.type:
                    return fail('%s but found %s'%(QueryParser.data_types[expected_type],QueryParser.data_types[variable.type]))
                return self.registered_vars[variable.value]
            else :
                return fail("Unkown variable %s"%variable.value)
        return variable_test_impl
    
    
    def assign_char_fn(self):
        @generate(' Assignment operator <<')
        def assign_char_impl():
            assign = yield string("<<")
            if assign is not None:
                return '<<'
            return fail('<<')
        return assign_char_impl
    
    
    def decimal_fn(self):
        @generate('Decimal number')
        def decimal_impl():
            number = yield regex(r"[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?")
            if number is not None:
                return QueryParser.var(6,float(number))
            return fail('valid floating number')
        return decimal_impl
    
    
    def integer_fn(self):
        @generate('Integer number')
        def integer_impl():
            number = yield regex(r"(?<!\.)\d+(?!\.)")
            if len(number)>0:
                return QueryParser.var(5,int(number))
            return fail('valid integer')
        return integer_impl
    
    
    def coor_point_fn(self):
        @generate('Point Coordinate') 
        def coor_point_impl():
            yield QueryParser.lexeme('(')
            lon = yield QueryParser.lexeme(self.decimal)
            yield QueryParser.lexeme(',')
            lat = yield QueryParser.lexeme(self.decimal)
            yield QueryParser.lexeme(')')
            
            if lat.value > 90 or lat.value < -90:
                return fail('Latitude in range -90 to 90')
            if lon.value >180 or lon.value < -180:
                return fail('Longitude in range -180 to 180')
            return QueryParser.var(7,(lon.value,lat.value))
        return coor_point_impl
    
    
    def polygon_fn(self):
        @generate('Polygon as point coordinates')
        def polygon_impl():
            sep = QueryParser.lexeme(string(','))
            poly = yield string('(') >> self.decimal.sep_by(sep) << string(')')
            if len(poly) % 2 != 0:
                return fail("A set of points multiple of 2, received %d"%len(poly))
            else:
                result = []
                for point in poly:
                    result.append(point.value)
                result.extend(result[0:2])
            return (8,result)
        return polygon_impl
    
    
    def type_value_fn(self):
        @generate
        def type_value_impl():
            return (yield (self.variable_test
                           |self.dir_test
                           |self.file_test
                           |self.date_test
                           |self.integer
                           |self.decimal
                           |self.coor_point
                           |self.polygon))
        return type_value_impl
    
    
    def assignment_test_fn(self):
        @generate
        def assignment_test_impl():
        
            reg_var = yield QueryParser.lexeme(self.identifier)
            yield QueryParser.lexeme(self.assign_char)
            type_value_ = yield self.type_value
            self.registered_vars[reg_var.value] = type_value_
        return assignment_test_impl
    
    
    def range_test_fn(self):
        @generate('Valid range Type(decimal, integer, or date)')
        def range_test_impl():
            yield QueryParser.lexeme(string("(")).optional()
            v1 = yield self.type_value
            literal_to = yield QueryParser.lexeme(string_from("to","TO",":")).result('TO')
            v2 = yield self.type_value
            yield QueryParser.lexeme(string(")")).optional()
            
            if v1.type != v2.type:
                return fail("Range values of the same type")                      
            
            c_v1= None
            c_v2= None
            
            if v1.type in [5,6]:
                c_v1 = min(v1.value,v2.value)
                c_v2 = max(v1.value,v2.value)
            elif v1.type == 7:
                d_v1 = datetime.strptime(v1.value,'%Y-%m-%dT%H:%M:%S:00.000Z')
                d_v2 = datetime.strptime(v2.value,'%Y-%m-%dT%H:%M:%S:00.000Z')
                if d_v1 > d_v2:
                    c_v1 = v2.value
                    c_v2 = v1.value
                else:
                    c_v1 = v1.value
                    c_v2 = v2.value
            else:
                return fail('Range of integers or floats or dates')
            
            return ['[',c_v1,literal_to,c_v2,']']
        return range_test_impl
                                                     
    def int_range(self):
        return self.range_test|QueryParser.lexeme(self.type_value).map(QueryParser.var_v)
    
    
    def op_fn(self):
        @generate("Valid expression with the operators 'AND' or 'OR' or 'NOT'")
        def op_impl():
            return (yield seq(self.simple(),QueryParser.lexeme(string("AND",transform=lambda l:l.lower())), QueryParser.lexeme(self.expr())) 
                    | seq(self.simple(),QueryParser.lexeme(string("OR",transform=lambda l:l.lower())), QueryParser.lexeme(self.expr())) 
                    | seq(QueryParser.lexeme(string("NOT",transform=lambda l:l.lower())),self.expr()))
        return op_impl
    
    def simple(self):
        return self.int_range() | seq(QueryParser.lexeme("("),self.op,QueryParser.lexeme(")"))
    
    def expr(self):
        return self.op|self.simple()
    
    def flatten(self,x):
        if not isinstance(x,list):
            return [x]
        if x == []:
            return x
        r = []
        for e in x:
            if isinstance(e,list):
                r+=self.flatten(e)
            else:
                r.append(e)
        return r
    
    def flatten_to_string(self,flat_list):
        flattened = self.flatten(flat_list)
        str_ = ""
        for el in flattened:
            if isinstance(el,QueryParser.var):
                str_+=str(el.value)+" "
            elif el:
                str_+=str(el)+" "
        if str_[-1] == ' ':
            return str_[0:-1]
        return str_
    
    def get_rel_date(self,rel_date_list):
        relative_date_map ={
            'MI':'MINUTES',
            'D':'DAYS',
            'H':'HOURS',
            'W':'WEEKS',
            'M':'MONTHS',
            'Y':'YEARS'
            }
        
        time_span = ''
        time_span_value = 0
        if len(rel_date_list[2]) <= 2:
            time_span = relative_date_map[rel_date_list[2]]
        else:
            time_span = rel_date_list[2]
        time_span_value = rel_date_list[1]
        
        if time_span == 'HOURS':
            time_span = 'MINUTES'
            time_span_value = rel_date_list[1]*60
        if time_span == 'YEARS':
            time_span = 'MONTHS'
            time_span_value = rel_date_list[1]*12
        elif time_span == 'WEEKS':
            time_span = 'WEEKS'
            time_span_value = rel_date_list[1]*7
            time_span = 'DAYS'
            
        delta_date = None
        if rel_date_list[0] == '-':
            time_span_value *= -1
        
        if time_span == 'MINUTES':
            delta_date = relativedelta(minutes=time_span_value)
        elif time_span == 'DAYS':
            delta_date = relativedelta(days=time_span_value)
        elif time_span == 'MONTHS':
            delta_date = relativedelta(months=time_span_value)
        
        return (rel_date_list[0]+str(time_span_value)+time_span,delta_date)
    
    
    def date_test_fn(self):
        @generate('Date')
        def date_test_impl():
            M_to_m = {
                'jan':1,
                'feb':2,
                'mar':3,
                'apr':4,
                'may':5,
                'jun':6,
                'jul':7,
                'aug':8,
                'sep':9,
                'oct':10,
                'nov':11,
                'dec':12
                }
            
            literal_today = string_from('NOW',transform=lambda l:l.upper()).result('NOW')
            relative_date = seq(QueryParser.lexeme(string_from('+','-').desc('Relative date')),
                                QueryParser.lexeme(self.integer.map(lambda var:var.value)),
                                QueryParser.lexeme(string_from('MI','MINUTES','H','HOURS','D','DAYS','W','WEEKS','M','MONTHS','Y','YEARS',
                                                        transform=lambda l:l.upper())))
            
            pot_day = regex('\d{1,2}') << regex('\s*(th|st|nd|rd)?(-|/|\s{1})?')
            pot_month = regex('(jan(uary)?)|(feb(ruary)?)|(mar(ch)?)|(apr(il)?)|(may)|(jun(e)?)|jul(ly)?|(aug(ust)?)|(sep(tember)?)|(oct(ober)?)|(nov(ember)?)|(dec(ember)?)') << regex('((-|/)|(,|\.)?\s)?')
            pot_month_day = regex('\d{1,2}(?!\d{2})') << regex('\s*(th|st|nd|rd)?(-|/|\s{1})?').optional()
            pot_year = regex('\d{2,4}')
        
            
            dmy_pot = seq(pot_month_day,pot_year)|seq(pot_year)
            
            pot_date = yield seq(literal_today,relative_date.optional())|seq(day=pot_day.optional(),month=pot_month.optional(),dmy=dmy_pot)
            
            if not isinstance(pot_date,dict):
                real_date = datetime.now()
                if pot_date[1] is not None:
                    rel_date_v = self.get_rel_date(pot_date[1]) 
                    real_date += rel_date_v[1]
                return QueryParser.var(7,real_date.strftime('%Y-%m-%dT%H:%M:%S:00.000Z'))
            
            month = 0
            m_from_d = False
            
            if pot_date['month'] is not None:
                month = int(M_to_m[pot_date['month'][0:3]])
            elif pot_date['day'] is not None and len(pot_date['dmy']) == 2:
                month = int(pot_date['dmy'][0])
            elif pot_date['day'] is not None and len(pot_date['dmy']) == 1:
                month = int(pot_date['day'])
                m_from_d = True
            else:
                return fail("clear month value")
            
            smonth = '%02d'%month
            
            day = 0
            
            if m_from_d == True:
                day = 1
            elif pot_date['day'] is not None:
                day = int(pot_date['day'])
            elif pot_date['day'] is None and len(pot_date['dmy']) == 2:
                day = int(pot_date['dmy'][0])
            else:
                day =1 
            
            sday = '%02d'%day
            
            syear= ''
            
            if len(pot_date['dmy']) == 2:
                syear = pot_date['dmy'][1]
            elif len(pot_date['dmy']) == 1:
                syear = pot_date['dmy'][0]
            else:
                return fail("clear year value")
            
            if(len(syear) == 2):
                syear = "20"+syear
            
            return QueryParser.var(7,syear+"-"+smonth+"-"+sday+"T00:00:00:00.000Z")
        return date_test_impl
    
    
    def req_footprint_test_fn(self):
        @generate
        def req_footprint_test_impl():
        
            yield QueryParser.lexeme(string_from('footprint','fp','aoi').result('footprint'))
            yield string_from('from','in','=',':')
            aoi = yield QueryParser.lexeme(self.coor_point|self.variable_test|self.file_test)
            
            if aoi.type == 9:
                return r'footprint:"Intersects(%f,%f)"'%(aoi.value[0],aoi.value[1])
            elif aoi.type == 10:
                return r'footprint:"Intersects(POLYGON(%s))"'%(','.join('%.5f'%v for v in aoi.value))
            elif aoi.type == 1:
                base=os.path.basename(aoi.value)
                _shp_file_ext = os.path.splitext(base)[1]
                if _shp_file_ext == '.shp':
                    convex_hull_wkt = shp_to_wkt(get_convex_hull(aoi.value,in_mem='y'))
                    return r'footprint:"Intersects(%s)"'%convex_hull_wkt
                elif _shp_file_ext == '.geojson':
                    wkt = geojson_to_wkt(aoi.value)
                    return r'footprint:"Intersects(%s)"'%wkt
                else:
                    return fail('Should be a valid shapefile path')
        return req_footprint_test_impl
    
    def req_discard_test_fn(self):
        @generate
        def req_discard_test_impl():
        
            yield QueryParser.lexeme(string_from('discard','ds','remove','rm'))
            yield string_from('from','in','=',':')
            discard_from = yield QueryParser.lexeme(self.variable_test|self.file_test)
            
            # if discard_from.type == 1:
            #     base=os.path.basename(discard_from.value)
            #     _file_ext = os.path.splitext(base)[1]
            #     if _file_ext == '.txt' or _file_ext == '.csv':
            #         convex_hull_wkt = shp_to_wkt(get_convex_hull(aoi.value,in_mem='y'))
            #     else:
            #         return fail('either a text or csv file')
            if discard_from.type == 0:
                self.discarded = [os.path.basename(os.path.splitext(filename)[0]) for filename in  glob.glob(os.path.join(discard_from.value,'*.zip'))]
        return req_discard_test_impl
    
    
    def req_platformname_test_fn(self):
        @generate
        def req_platformname_test_impl():
            valid_pfnames = ['Sentinel-1','Sentinel-2','Sentinel-3','Sentinel-5 Precursor']
            order_by_ = yield QueryParser.lexeme(regex('[+-](?=[a-zA-Z])').optional())
            yield QueryParser.lexeme(string_from('platformname','pfname','pfn').result('platformname'))
            yield QueryParser.lexeme(string_from('from','in','=',':'))
            pfname = yield (regex('[sS].*1').result(valid_pfnames[0])|regex('[sS].*2').result(valid_pfnames[1])|regex('[sS].*3').result(valid_pfnames[2])|regex('[sS].*5').result(valid_pfnames[3]))
            if pfname not in valid_pfnames:
                return fail('one of Sentinel-1, Sentinel-2, Sentinel-3, or Sentinel-5 Precursor')
            if (order_by_):
                if order_by_ == '+':
                    self.order_by.append('platformname asc')
                else:
                    self.order_by.append('platformname desc')
                    
            return('platformname:%s'%pfname)
        return req_platformname_test_impl
    
    
    def req_bydate_test_fn(self):
        @generate
        def req_bydate_test_impl():
            order_by_ = yield QueryParser.lexeme(regex('[+-](?=[a-zA-Z])').optional())
            criteria = yield (QueryParser.lexeme(string_from('beginposition','bp').result('beginposition'))
                             |QueryParser.lexeme(string_from('endposition','ep').result('endposition'))
                             |QueryParser.lexeme(string_from('ingestiondate','id').result('ingestiondate'))
                                ).desc('Requires one of: beginposition|bp, endposition|ep, ingestiondate|id')
            yield string_from('from','in','=',':')
            date_range = yield QueryParser.lexeme(self.expr())
            date_range_str = self.flatten_to_string(date_range)
            
            if (order_by_):
                if order_by_ == '+':
                    self.order_by.append('%s asc'%criteria)
                else:
                    self.order_by.append('%s desc'%criteria)
            
            return('%s:%s'%(criteria,date_range_str))
        return req_bydate_test_impl
    
    
    def req_filename_test_fn(self):
        @generate
        def req_filename_test_impl():
            order_by_ = yield QueryParser.lexeme(regex('[+-](?=[a-zA-Z])').optional())
            yield (QueryParser.lexeme(string_from('filename','fname','fn')))
            yield string_from('from','in','=',':')
            filename = yield QueryParser.lexeme(self.string_test)
            if (order_by_):
                if order_by_ == '+':
                    self.order_by.append('filename asc')
                else:
                    self.order_by.append('filename desc')
            return('filename:%s'%filename.value)
        return req_filename_test_impl
    
    
    def req_orbitnumber_test_fn(self):
        @generate
        def req_orbitnumber_test_impl():
            order_by_ = yield QueryParser.lexeme(regex('[+-](?=[a-zA-Z])').optional())
            criteria = yield (QueryParser.lexeme(string_from('orbitnumber','on').result('orbitnumber'))
                             |QueryParser.lexeme(string_from('lastorbitnumber','lon').result('lastorbitnumber'))
                             |QueryParser.lexeme(string_from('relativeorbitnumber','ron').result('relativeorbitnumber'))
                             |QueryParser.lexeme(string_from('lastrelativeorbitnumber','lron').result('lastrelativeorbitnumber'))
                                ).desc('Requires one of: orbitnumber|on, lastorbitnumber|lon, relativeorbitnumber|ron, lastrelativeorbitnumber|lron')
            yield string_from('from','in','=',':')
            on_range = yield QueryParser.lexeme(self.expr())
            on_range_str = self.flatten_to_string(on_range)
            if (order_by_):
                if order_by_ == '+':
                    self.order_by.append('%s asc'%criteria)
                else:
                    self.order_by.append('%s desc'%criteria)
            return('%s:%s'%(criteria,on_range_str))
        return req_orbitnumber_test_impl
    
    
    def req_orbitdirection_test_fn(self):
        @generate
        def req_orbitdirection_test_impl():
            order_by_ = yield QueryParser.lexeme(regex('[+-](?=[a-zA-Z])').optional())
            criteria = yield (QueryParser.lexeme(string_from('orbitdirection','odir','od').result('orbitdirection')))
            yield string_from('from','in','=',':')
            odir = yield QueryParser.lexeme(string_from('ascending','asc','a').result('Ascending')|string_from('descending','des','d').result('Descending')).desc('Requires one of: ascending|asc|a OR descending|des|d')
            if (order_by_):
                if order_by_ == '+':
                    self.order_by.append('%s asc'%criteria)
                else:
                    self.order_by.append('%s desc'%criteria)
            return ('%s:%s'%(criteria,odir))
        return req_orbitdirection_test_impl
    
    
    def req_polarmode_test_fn(self):
        @generate
        def req_polarmode_test_impl():
            order_by_ = yield QueryParser.lexeme(regex('[+-](?=[a-zA-Z])').optional())
            criteria = yield (QueryParser.lexeme(string_from('polarisationmode','pmode','pm').result('polarisationmode')))
            yield string_from('from','in','=',':')
            pmode = yield(QueryParser.lexeme(string_from('HH', 'VV', 'HV', 'VH', 'HH HV', 'VV VH',transform=lambda l:l.lower())))
            if (order_by_):
                if order_by_ == '+':
                    self.order_by.append('%s asc'%criteria)
                else:
                    self.order_by.append('%s desc'%criteria)
            return '%s:%s'%(criteria,pmode)
        return req_polarmode_test_impl
    
    
    def req_producttype_test_fn(self):
        @generate
        def req_producttype_test_impl():
            order_by_ = yield QueryParser.lexeme(regex('[+-](?=[a-zA-Z])').optional())
            criteria = yield (QueryParser.lexeme(string_from('producttype','ptype','pt').result('producttype')))
            yield string_from('from','in','=',':')
            ptype = yield(QueryParser.lexeme(string_from('SLC','GRD','OCN',transform=lambda l:l.lower())))
            if (order_by_):
                if order_by_ == '+':
                    self.order_by.append('%s asc'%criteria)
                else:
                    self.order_by.append('%s desc'%criteria)
            return '%s:%s'%(criteria,ptype)
        return req_producttype_test_impl
    
    
    def req_cloudcover_test_fn(self):
        @generate
        def req_cloudcover_test_impl():
            order_by_ = yield QueryParser.lexeme(regex('[+-](?=[a-zA-Z])').optional())
            criteria = yield (QueryParser.lexeme(string_from('cloudcoverpercentage','cloudcover','cloudcover%','ccp').result('cloudcoverpercentage')))
            yield string_from('from','in','=',':')
            ccp = yield(QueryParser.lexeme(self.expr()))
            ccp_string = self.flatten_to_string(ccp)
            if (order_by_):
                if order_by_ == '+':
                    self.order_by.append('%s asc'%criteria)
                else:
                    self.order_by.append('%s desc'%criteria)
            return '%s:%s'%(criteria,ccp_string)
        return req_cloudcover_test_impl
    
    def requests_test(self):
        return (
            self.assignment_test << QueryParser.line_end
            | self.req_footprint_test << QueryParser.line_end
            | self.req_platformname_test << QueryParser.line_end
            | self.req_bydate_test << QueryParser.line_end
            | self.req_filename_test << QueryParser.line_end
            | self.req_orbitnumber_test << QueryParser.line_end
            | self.req_polarmode_test << QueryParser.line_end
            | self.req_producttype_test << QueryParser.line_end
            | self.req_cloudcover_test << QueryParser.line_end
            | self.req_discard_test << QueryParser.line_end
            ).many()

    def parse(self,query_str):
        
        self.registered_vars.clear()
        self.registered_criteria.clear()
        self.order_by.clear()
        
        intermediate_parsed_request = self.requests_test().parse(query_str)
        parsed_request =''
        for i in range(len(intermediate_parsed_request)):
            if intermediate_parsed_request[i]:
                parsed_request += "(%s)"%intermediate_parsed_request[i]
                if (i < len(intermediate_parsed_request)-2
                    or (i == len(intermediate_parsed_request)-2 and intermediate_parsed_request[-1])
                    ) :
                    parsed_request += ' AND '
                    
        self.query_string = parsed_request
        self.query_order = self.order_by
        self.is_query_sucess = True
        self.discarded_from_query = self.discarded
        
        return parsed_request,self.order_by,self.discarded