"""
Created on Mon Jan 27 17:37:57 2020

@author: ibrahim.fayad

some function adapted from : https://github.com/sentinelsat/sentinelsat
"""
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function

import requests
from six import string_types
from datetime import datetime,timedelta
from six.moves.urllib.parse import urljoin
from collections import OrderedDict
from .requestparser import QueryParser
import html2text
from parsy import ParseError


class SentinelQuerist:
    
    __version_mjaor__ = 0
    __version_minor__ = 1
    __version_micro__ = 7
    __version__ = "{}.{}.{}".format(__version_mjaor__, __version_minor__, __version_micro__)

    
    def __init__(
        self,
        user,
        password,
        api_url="https://apihub.copernicus.eu/apihub/",
        timeout=None,
    ):
        self.session = requests.Session()
        if user and password:
            self.session.auth = (user, password)
        self.api_url = api_url if api_url.endswith("/") else api_url + "/"
        self.page_size = 100
        self.user_agent = "sentinelsat/" + self.__version__
        self.session.headers["User-Agent"] = self.user_agent
        self.timeout = timeout
        self.query_parser = QueryParser()

    def query_check(self,query):
        try:
            parsed_query,order_by,discarded = self.query_parser.parse(query)
            products, count = self._load_subquery(parsed_query)
            return count
            
        except ParseError as pe:
            parsing_error = ParsingError(str(pe),pe.stream)
            raise parsing_error
        
    
    def query_yield(self, query,limit=None,offset=0):
        
        try:
            if not self.query_parser.is_query_success:
                parsed_query,order_by,discarded = self.query_parser.parse(query)
            else:
                parsed_query = self.query_parser.query_string
                order_by = self.query_parser.query_order
                discarded = self.discareded_from_string
            
            formatted_order_by = None
            if order_by and isinstance(order_by,list) and len(order_by)>0:
                formatted_order_by = ",".join(order_by)
                
            
            for response_part,count in self._load_query(parsed_query, formatted_order_by, limit, offset):
                yield self._parse_opensearch_response(response_part,discard_list=discarded),len(response_part),count
        except ParseError as pe:
            parsing_error = ParsingError(str(pe),pe.stream)
            raise parsing_error

    def _load_subquery(self, query, order_by=None, limit=None, offset=0):
    
        # load query results
        url = self._format_url(order_by, limit, offset)
        response = self.session.post(
            url,
            {"q": query},
            auth=self.session.auth,
            headers={"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"},
            timeout=150,
        )
    
        # parse response content
        try:
            json_feed = response.json()["feed"]
            if json_feed["opensearch:totalResults"] is None:
                # We are using some unintended behavior of the server that a null is
                # returned as the total results value when the query string was incorrect.
                raise Exception(
                    "Invalid query string. Check the parameters and format.", response
                )
            total_results = int(json_feed["opensearch:totalResults"])
        except (ValueError, KeyError):
            raise Exception("API response not valid. JSON decoding failed.", response)
    
        products = json_feed.get("entry", [])
        # this verification is necessary because if the query returns only
        # one product, self.products will be a dict not a list
        if isinstance(products, dict):
            products = [products]
    
        return products, total_results

    def _format_url(self, order_by=None, limit=None, offset=0):
        if limit is None:
            limit = self.page_size
        limit = min(limit, self.page_size)
        url = "search?format=json&rows={}".format(limit)
        url += "&start={}".format(offset)
        if order_by:
            url += "&orderby={}".format(order_by)
        return urljoin(self.api_url, url)


    def _load_query(self, query, order_by=None, limit=None, offset=0):
        products, count = self._load_subquery(query, order_by, limit, offset)
        yield products,count
        # repeat query until all results have been loaded
        max_offset = count
        if limit is not None:
            max_offset = min(count, offset + limit)
            
        if max_offset > offset + self.page_size:
            for new_offset in range(offset + self.page_size, max_offset, self.page_size):
                new_limit = limit
                if limit is not None:
                    new_limit = limit - new_offset + self.offset
                ret = self._load_subquery(query, order_by, new_limit, new_offset)[0]
                # products += ret
                products = ret
                yield products,count
        else:
            yield products,count
    

    def _parse_opensearch_response(self, products,discard_list):
        """Convert a query response to a dictionary.
        The resulting dictionary structure is {<product id>: {<property>: <value>}}.
        The property values are converted to their respective Python types unless `parse_values`
        is set to `False`.
        """
    
        converters = {"date": _parse_iso_date, "int": int, "long": int, "float": float, "double": float}
        # Keep the string type by default
        default_converter = lambda x: x
    
        output = OrderedDict()
        for prod in products:
            if (discard_list 
                and isinstance(discard_list,list) 
                and len(discard_list) >0
                and prod['title'] in discard_list):
                continue
            
            product_dict = {}
            prod_id = prod["id"]
            output[prod_id] = product_dict
            for key in prod:
                if key == "id":
                    continue
                if isinstance(prod[key], string_types):
                    product_dict[key] = prod[key]
                else:
                    properties = prod[key]
                    if isinstance(properties, dict):
                        properties = [properties]
                    if key == "link":
                        for p in properties:
                            name = "link"
                            if "rel" in p:
                                name = "link_" + p["rel"]
                            product_dict[name] = p["href"]
                    else:
                        f = converters.get(key, default_converter)
                        for p in properties:
                            try:
                                product_dict[p["name"]] = f(p["content"])
                            except KeyError:
                                # Sentinel-3 has one element 'arr'
                                # which violates the name:content convention
                                product_dict[p["name"]] = f(p["str"])
        return output
        
    def get_product_odata(self, id, full=False):
        """Access OData API to get info about a product.
        Returns a dict containing the id, title, size, md5sum, date, footprint and download url
        of the product. The date field corresponds to the Start ContentDate value.
        If `full` is set to True, then the full, detailed metadata of the product is returned
        in addition to the above.
        Parameters
        ----------
        id : string
            The UUID of the product to query
        full : bool
            Whether to get the full metadata for the Product. False by default.
        Returns
        -------
        dict[str, Any]
            A dictionary with an item for each metadata attribute
        Notes
        -----
        For a full list of mappings between the OpenSearch (Solr) and OData attribute names
        see the following definition files:
        https://github.com/SentinelDataHub/DataHubSystem/blob/master/addon/sentinel-1/src/main/resources/META-INF/sentinel-1.owl
        https://github.com/SentinelDataHub/DataHubSystem/blob/master/addon/sentinel-2/src/main/resources/META-INF/sentinel-2.owl
        https://github.com/SentinelDataHub/DataHubSystem/blob/master/addon/sentinel-3/src/main/resources/META-INF/sentinel-3.owl
        """
        url = urljoin(self.api_url, "odata/v1/Products('{}')?$format=json".format(id))
        if full:
            url += "&$expand=Attributes"
        response = self.session.get(url, auth=self.session.auth, timeout=self.timeout)
        _check_scihub_response(response)
        values = _parse_odata_response(response.json()["d"])
        return values
            
class SentinelQueristError(Exception):
    """Invalid responses from DataHub.
    Attributes
    ----------
    msg: str
        The error message.
    response: requests.Response
        The response from the server as a `requests.Response` object.
    """

    def __init__(self, msg=None, response=None):
        self.msg = msg
        self.response = response

    def __str__(self):
        return "HTTP status {0} {1}: {2}".format(
            self.response.status_code,
            self.response.reason,
            ("\n" if "\n" in self.msg else "") + self.msg,
        )

class ParsingError(Exception):
    def __init__(self,msg=None,stream=None):
        at_loc = msg.rfind("at")
        semi_loc = msg[at_loc:].find(":") + at_loc
        error = msg[:at_loc]
        row = int(msg[at_loc+2:semi_loc])
        col = int(msg[semi_loc+1:])
        self.msg = error
        self.row = row
        self.col = col
        self.stream =  stream.splitlines()[self.row]
        self.error_pos = ' '*len(self.stream)
        self.error_pos = self.error_pos[0:col] + '^' + self.error_pos[col+1:]
        
    def __str__(self):
        return "Parsing Error in Line: {0}, Column: {1}\n>>{2}\n  {3}\nReason: {4}".format(
            self.row+1,
            self.col+1,
            self.stream,
            self.error_pos,
            self.msg
            )

def _parse_odata_response(product):
    output = {
        "id": product["Id"],
        "title": product["Name"],
        "size": int(product["ContentLength"]),
        product["Checksum"]["Algorithm"].lower(): product["Checksum"]["Value"],
        "date": _parse_odata_timestamp(product["ContentDate"]["Start"]),
        "url": product["__metadata"]["media_src"],
        "Online": product.get("Online", True),
        "Creation Date": _parse_odata_timestamp(product["CreationDate"]),
        "Ingestion Date": _parse_odata_timestamp(product["IngestionDate"]),
    }
    # Parse the extended metadata, if provided
    converters = [int, float, _parse_iso_date]
    for attr in product["Attributes"].get("results", []):
        value = attr["Value"]
        for f in converters:
            try:
                value = f(attr["Value"])
                break
            except ValueError:
                pass
        output[attr["Name"]] = value
    return output

def _parse_odata_timestamp(in_date):
    """Convert the timestamp received from OData JSON API to a datetime object.
    """
    timestamp = int(in_date.replace("/Date(", "").replace(")/", ""))
    seconds = timestamp // 1000
    ms = timestamp % 1000
    return datetime.utcfromtimestamp(seconds) + timedelta(milliseconds=ms)

def _parse_iso_date(content):
    if "." in content:
        return datetime.strptime(content, "%Y-%m-%dT%H:%M:%S.%fZ")
    else:
        return datetime.strptime(content, "%Y-%m-%dT%H:%M:%SZ")
    
def _check_scihub_response(response, test_json=True):
        """Check that the response from server has status code 2xx and that the response is valid JSON.
        """
        # Prevent requests from needing to guess the encoding
        # SciHub appears to be using UTF-8 in all of their responses
        response.encoding = "utf-8"
        try:
            response.raise_for_status()
            if test_json:
                response.json()
        except (requests.HTTPError, ValueError):
            msg = "Invalid API response."
            try:
                msg = response.headers["cause-message"]
            except:
                try:
                    msg = response.json()["error"]["message"]["value"]
                except:
                    if not response.text.strip().startswith("{"):
                        try:
                            h = html2text.HTML2Text()
                            h.ignore_images = True
                            h.ignore_anchors = True
                            msg = h.handle(response.text).strip()
                        except:
                            pass
            api_error = SentinelQueristError(msg, response)
            # Suppress "During handling of the above exception..." message
            # See PEP 409
            api_error.__cause__ = None
            raise api_error
