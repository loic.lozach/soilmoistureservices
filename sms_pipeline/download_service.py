#!/usr/bin/python
import os, sys, argparse
#sys.path.append("/work/python")
from SentinelDM import SenTUI as tui
from subprocess import Popen, PIPE

sq = r"""shp << '/data/S2Tiles/T30UVU.shp';
aoi in shp;
bp from NOW to NOW-3d;
pfname = s1;
ptype = GRD;
"""

def process_command(cmd):
#     my_env = os.environ.copy()
#     my_env["PATH"] = "/work/python/theia_download:" + my_env["PATH"]    , env=my_env
    print("Starting : "+" ".join(cmd))
    p = Popen(cmd, stdout=PIPE, cwd="/work/python/theia_download")
#    p.wait()
    output = p.communicate()[0]
    if p.returncode != 0: 
        print("process failed %d : %s" % (p.returncode, output))
        exit()
    print(output)
    return p.returncode

if __name__ == "__main__":
    
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Download services
        """)

    subparsers = parser.add_subparsers(help='Choose server', dest="pipeline")

    # Short pipeline
    list_parser = subparsers.add_parser('scihub', help="Download from Copernicus Scihub server")
    
    list_parser = subparsers.add_parser('theia', help="Download S2 L3A over France from Theia-Land server")
    list_parser.add_argument('-tile', action='store', required=True, help="Sentinel2 Tile name ex: T30UVU")
    list_parser.add_argument('-start', action='store', required=True, help='Starting date, ex. 2019-01-01')
    list_parser.add_argument('-end', action='store', required=True, help='Ending date, ex. 2019-12-31')
    list_parser.add_argument('-outdir', action='store', required=True, help='Output directory')


    args=parser.parse_args()
    
    if args.pipeline == 'scihub' :
        os.system('mode con: cols={} lines={}'.format(140,40))
        main = tui.MainTUI(query_string=sq) #query_string=sq
        main.run()
    elif args.pipeline == 'theia' :
        outabs = os.path.abspath(args.outdir)
        if not os.path.exists(outabs):
            os.makedirs(outabs)
        
        cmd = ['python', 'theia_download.py', '-t', args.tile, '-a', 'config_theia.cfg', \
               '-d', args.start, '-f', args.end, '--level', 'LEVEL3A', '-w', outabs]
        
        process_command(cmd)
        
        
        
        
